package vmine

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"
)

var (
	// Global image registry.
	imageRegistryMx sync.Mutex
	imageRegistry   map[string]*Image

	// Name of the default image.
	defaultImageName string
)

// We want to treat system operations as they were running in
// "transactions". We think of the operation of running a Group as a
// sequence of setup / teardown tasks, combined with long-running
// operations that must be watched. What we need to do is ensure that
//
// - long-running watched tasks are part of an errgroup, any failure
//   should cause an abort;
//
// - teardown tasks are called in all cases;
//
// - setup / teardown tasks need to be executed in order, parallelism
//   is desired only for the "running" stage.
//
// Since the configuration is fully static (not changed at runtime),
// we can control it in-memory with goroutines and errgroups, but it
// is good anyway for readability to standardize an interface that
// will be used to call setup / run / teardown methods on the various
// entities.

// The Transaction is the abstraction we use to collect and execute
// setup / teardown tasks and running processes. Calls to Setup() and
// Run() just gather information, which is only applied on Execute().
type Transaction interface {
	Setup(SetupTask)
	Run(RunnableTask)

	// There's no rollback, as nothing happens until you call
	// Execute() anyway, and it takes care of undoing all the
	// steps that were performed up to the failure.
	Execute(context.Context) error
}

// SetupTask interface, with separate setup / teardown steps.
type SetupTask interface {
	Start(context.Context) error
	Stop() error
}

// RunnableTask is an interface to a task that can be run, waiting
// until its termination.
type RunnableTask interface {
	Run(context.Context) error
}

// Our Transaction implementation.
type tx struct {
	setupTasks []SetupTask
	runTasks   []RunnableTask
}

func newTX() *tx {
	return new(tx)
}

func (t *tx) Setup(task SetupTask) {
	t.setupTasks = append(t.setupTasks, task)
}

func (t *tx) Run(task RunnableTask) {
	t.runTasks = append(t.runTasks, task)
}

func (t *tx) Execute(ctx context.Context) (outErr error) {
	var setupStack []SetupTask

	// Teardown function. Iterate over setupStack in reverse and
	// call Stop() on all tasks.
	defer func() {
		for i := len(setupStack) - 1; i >= 0; i-- {
			// Ignore errors on teardown unless outErr is
			// nil, in which case we report a teardown
			// error (but proceed anyway).
			err := setupStack[i].Stop()
			if err != nil && outErr == nil {
				outErr = fmt.Errorf("teardown error: %w", err)
			}
		}
	}()

	// Call the Start() method on all setup tasks in sequence.
	for _, task := range t.setupTasks {
		if err := task.Start(ctx); err != nil {
			outErr = err
			return
		}
		setupStack = append(setupStack, task)
	}

	// Run all the runnable tasks in a single errgroup.
	eg, ectx := errgroup.WithContext(ctx)
	for _, task := range t.runTasks {
		// Capture the current task in a closure.
		func(t RunnableTask) {
			eg.Go(func() error {
				return t.Run(ectx)
			})
		}(task)
	}

	// Wait.
	outErr = eg.Wait()
	return
}

// Just an adapter for os/exec.Cmd that creates a RunnableTask.
type cmdTask struct {
	name string
	args []string
}

func newCmd(name string, args ...string) RunnableTask {
	return &cmdTask{
		name: name,
		args: args,
	}
}

func (c *cmdTask) Run(ctx context.Context) error {
	log.Printf("running: %s %s", c.name, strings.Join(c.args, " "))
	return withStdout(exec.CommandContext(ctx, c.name, c.args...)).Run()
}

// Shell-based SetupTask, with separate setup / teardown scripts.
type shellSetupTask struct {
	startScript, stopScript string
}

func newShellSetupTask(start, stop string, vars map[string]string) SetupTask {
	return &shellSetupTask{
		startScript: renderTemplate(start, vars),
		stopScript:  renderTemplate(stop, vars),
	}
}

func (s *shellSetupTask) Start(ctx context.Context) error {
	log.Printf("executing setup task: %s", s.startScript)
	return withStdout(
		exec.CommandContext(ctx, "/bin/sh", "-c", s.startScript),
	).Run()
}

func (s *shellSetupTask) Stop() error {
	log.Printf("executing teardown task: %s", s.stopScript)
	return withStdout(
		exec.Command("/bin/sh", "-c", s.stopScript),
	).Run()
}

func withStdout(cmd *exec.Cmd) *exec.Cmd {
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd
}

// The Engine keeps everything running.
//
// All spawned processes are monitored by the WaitGroup. We also
// increment the WaitGroup counter when creating the Engine itself, so
// it won't terminate before receiving CreateGroup requests. The call
// to Stop() then decrements the initial WaitGroup counter, allowing
// us to wait for all the processing threads to stop cleanly.
type Engine struct {
	memoryLimit int

	wg          sync.WaitGroup
	mx          sync.Mutex
	groups      map[string]*runningGroup
	memoryUsage int
}

type runningGroup struct {
	*Group
	cancel context.CancelFunc
}

// NewEngine creates a new Engine.
func NewEngine(memoryLimit int) *Engine {
	e := &Engine{
		memoryLimit: memoryLimit,
		groups:      make(map[string]*runningGroup),
	}
	e.wg.Add(1)
	return e
}

// Stop the Engine and all associated resources.
func (e *Engine) Stop() {
	e.mx.Lock()
	for _, rg := range e.groups {
		rg.cancel()
	}
	e.mx.Unlock()

	e.wg.Done()
}

// Wait until the Engine terminates.
func (e *Engine) Wait() {
	e.wg.Wait()
}

// CreateGroup creates and starts a new vm group.
func (e *Engine) CreateGroup(req *CreateGroupRequest) (*CreateGroupResponse, error) {
	group, err := newGroup(req)
	if err != nil {
		return nil, err
	}

	// Create a transaction and build the resources.
	tx := newTX()
	if err := group.Build(tx); err != nil {
		return nil, err
	}

	// To execute the transaction we need a controlling Context
	// that we can stop. Keep track of these in our 'groups' map,
	// so we can stop a group on request. Automatically unregister
	// from 'groups' once the transaction terminates. The
	// controlling Context is where we set the TTL, so it is
	// automatically canceled on expiration.
	ttl := time.Duration(req.TTL)
	if ttl == 0 {
		ttl = 1 * time.Hour
	}
	ctx, cancel := context.WithTimeout(context.Background(), ttl)

	// Checking if the memory is available and agreeing to start
	// the group should both happen while the lock is held.
	e.mx.Lock()
	ram := group.ram()
	if avail := e.memoryLimit - e.memoryUsage; e.memoryLimit > 0 && avail < ram {
		err = fmt.Errorf("not enough available memory (%dM free, %dM needed)", avail, ram)
	} else {
		e.groups[group.ID] = &runningGroup{
			Group:  group,
			cancel: cancel,
		}
		e.memoryUsage += ram
	}
	e.mx.Unlock()
	if err != nil {
		return nil, err
	}

	log.Printf("starting group %s (%d hosts, network=%s, ttl=%s)",
		group.ID, len(group.VMs), group.Network.NetworkCIDR(), ttl)

	e.wg.Add(1)
	go func() {
		err := tx.Execute(ctx)
		if err != nil {
			log.Printf("group %s terminated with error: %v", group.ID, err)
		} else {
			log.Printf("group %s terminated successfully", group.ID)
		}

		e.mx.Lock()
		delete(e.groups, group.ID)
		e.memoryUsage -= ram
		e.mx.Unlock()

		e.wg.Done()
	}()

	return &CreateGroupResponse{
		GroupID: group.ID,
	}, nil
}

// StopGroup terminates a running vm group. The method returns
// immediately and does not wait for resource teardown.
func (e *Engine) StopGroup(id string) error {
	e.mx.Lock()
	defer e.mx.Unlock()

	rg, ok := e.groups[id]
	if !ok {
		return errors.New("group not found")
	}
	log.Printf("terminating group %s", id)
	rg.cancel()
	return nil
}

// LoadImageRegistry initializes the global VM image registry.
func LoadImageRegistry(data map[string]*Image, dflt string) {
	imageRegistryMx.Lock()
	imageRegistry = data
	defaultImageName = dflt
	imageRegistryMx.Unlock()
}

func getImage(name string) (*Image, bool) {
	imageRegistryMx.Lock()
	defer imageRegistryMx.Unlock()
	img, ok := imageRegistry[name]
	return img, ok
}
