VMine
===

*VMine* (in Italian, "tiny VMs") is a Virtual Machine Manager like
*libvirt*, *Xen*, *Ganeti* etc, except a lot simpler and dumber. It is
intended for a specific use case: managing *ephemeral*, *short-lived*
virtual machines for use in systems integration testing, generally in
the context of a Continuous Integration environment. It uses QEmu/KVM
under the hood.

The service offers an HTTP API which allows you to create a group of
VMs attached to a shared private network (so they can talk to each
other), which is routed to the Internet (to download packages
etc). The network parameters are selected, randomly, by the
caller. Creating a VM group returns a unique ID that can be used to
shut it down when you're done. That's it, that is the totality of the
API.

You can choose a small number of runtime parameters for the virtual
machines: RAM, CPU, and which base image to use. Images are defined in
an *image registry*, which is just a YAML file with a list of named
rootfs / kernel / initrd images. Since these are *ephemeral* VMs, the
rootfs image will be mounted read-only, with changes going to a
temporary file which is then discarded.

## Running the vmine daemon

The *vmine* daemon needs to run as root (or at the very least it needs
the CAP_NET_ADMIN, and possibly other, capabilities), as it will make
changes to the network configuration of the host system, creating
bridge and TAP interfaces and firewall rules. It does however make an
effort to do this *properly*, with matched setup / teardown steps, so
it will leave your network configuration as it found it, on graceful
termination. The intention is for vmine to leak no resources on the
long term, even in case of errors.

The only requirement is that the host system should have IPv4
forwarding enabled:

```shell
$ echo 1 > /proc/sys/net/ipv4/ip_forward
```

In order to start the daemon, you have to build it first. Clone this
repository locally, then build the code (set GO111MODULE=on if you
have a Go version older than 1.15):

```shell
$ go build ./cmd/vmine
```

The only required configuration is the *image registry* file, see the
"VM Images" section below for a description of its format. Once you
have that, start the daemon with:

```shell
$ sudo ./vmine --images=my-images.yml
```

This will start the daemon, with the HTTP API endpoint listening on
port 4949.

## VM lifecycle

All virtual machines in a group share the same fate: if one
terminates, the entire group is turned down. They also all share the
fate of the vmine daemon: when it is stopped or restarted, all groups
are shut down.

All groups have a TTL (which by default is 1 hour if not explicitly
specified), after which they are shut down automatically. This is to
avoid resource leaks when clients "forget" to call the shutdown API.

The boot strategy implemented should allow VMs to become available
within a few seconds after they've been started.

## VM Images

VMine can't run on arbitrary VM images because it needs some
cooperation from the VM in order to configure the network and other
host parameters. The changes required are actually very simple and can
be examined by looking at the *aux/post-install.sh*
script. Fortunately it's pretty easy to build your own base VM images,
see the "Building an image" section below.

Also, since we don't use the BIOS to boot, every image needs to be
accompanied by its kernel / initrd image pair, stored separately from
the image itself.

The *image registry* is just a YAML file containing pointers to the
set of rootfs / kernel / initrd files for all available VM
images. This is stored as a dictionary of *name*: *paths* pairs, along
with an explicit indication of the default image to use when clients
don't explicitly specify one.

A valid image registry file could look like this:

```yaml
images:
  buster:
    path: "/images/buster/rootfs.img"
    kernel_path: "/images/buster/vmlinuz-4.19.0-14-amd64"
    initrd_path: "/images/buster/initrd.img-4.19.0-14-amd64"
  bullseye:
    path: "/images/bullseye/rootfs.img"
    kernel_path: "/images/bullseye/vmlinuz-5.10.0-1-amd64"
    initrd_path: "/images/bullseye/initrd.img-5.10.0-1-amd64"
default: "buster"
```

which defines two images, *buster* and *bullseye*, and sets the
default to *buster*.

### Download pre-built images

For convenience, we build Debian stable / nextstable images weekly
with this project's CI. You can download these images from the
project's [Package Registry](https://git.autistici.org/ai3/tools/vmine/-/packages).

### Building an image

VMine can only work with QCow2 rootfs images.

Simple steps for Debian-based distributions with *autopkgtest*:

```shell
$ IMG=/path/to/rootfs.img
$ sudo apt install autopkgtest vmdb2 lsof
$ sudo autopkgtest-build-qemu \
    buster $IMG http://deb.debian.org/debian amd64 $PWD/aux/post-install.sh
```

If you want to use a caching proxy for the installation phase, use the
*http_proxy* env var, with a http:// prefix (just host:port won't
work), e.g.:

```shell
$ export http_proxy=http://127.0.0.1:3142
```

Note that this will **not** result in having a proxy configured in the
resulting image, it will only be used to speed up debootstrap. You
should use the AUTOPKGTEST_APT_PROXY env var for that (check out the
*autopkgtest-build-qemu* manpage).

Then you can extract the kernel image and initrd from the generated
QCow image. The procedure is automated with a script:

```shell
$ sudo ./aux/extract-kernel.sh $IMG .
```

The above command will drop vmlinuz and initrd from the previously
generated image into the current directory.

### Image setup

The VM images generated with the above procedure are bare-bones Debian
installations with no customization beyond network configuration.

There is no console access, you're expected to connect over SSH using
the following well-known SSH key, which happens to be the default
Vagrant "insecure" SSH key distributed with the Vagrant packages:

```
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzI
w+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoP
kcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2
hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NO
Td0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcW
yLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQIBIwKCAQEA4iqWPJXtzZA68mKd
ELs4jJsdyky+ewdZeNds5tjcnHU5zUYE25K+ffJED9qUWICcLZDc81TGWjHyAqD1
Bw7XpgUwFgeUJwUlzQurAv+/ySnxiwuaGJfhFM1CaQHzfXphgVml+fZUvnJUTvzf
TK2Lg6EdbUE9TarUlBf/xPfuEhMSlIE5keb/Zz3/LUlRg8yDqz5w+QWVJ4utnKnK
iqwZN0mwpwU7YSyJhlT4YV1F3n4YjLswM5wJs2oqm0jssQu/BT0tyEXNDYBLEF4A
sClaWuSJ2kjq7KhrrYXzagqhnSei9ODYFShJu8UWVec3Ihb5ZXlzO6vdNQ1J9Xsf
4m+2ywKBgQD6qFxx/Rv9CNN96l/4rb14HKirC2o/orApiHmHDsURs5rUKDx0f9iP
cXN7S1uePXuJRK/5hsubaOCx3Owd2u9gD6Oq0CsMkE4CUSiJcYrMANtx54cGH7Rk
EjFZxK8xAv1ldELEyxrFqkbE4BKd8QOt414qjvTGyAK+OLD3M2QdCQKBgQDtx8pN
CAxR7yhHbIWT1AH66+XWN8bXq7l3RO/ukeaci98JfkbkxURZhtxV/HHuvUhnPLdX
3TwygPBYZFNo4pzVEhzWoTtnEtrFueKxyc3+LjZpuo+mBlQ6ORtfgkr9gBVphXZG
YEzkCD3lVdl8L4cw9BVpKrJCs1c5taGjDgdInQKBgHm/fVvv96bJxc9x1tffXAcj
3OVdUN0UgXNCSaf/3A/phbeBQe9xS+3mpc4r6qvx+iy69mNBeNZ0xOitIjpjBo2+
dBEjSBwLk5q5tJqHmy/jKMJL4n9ROlx93XS+njxgibTvU6Fp9w+NOFD/HvxB3Tcz
6+jJF85D5BNAG3DBMKBjAoGBAOAxZvgsKN+JuENXsST7F89Tck2iTcQIT8g5rwWC
P9Vt74yboe2kDT531w8+egz7nAmRBKNM751U/95P9t88EDacDI/Z2OwnuFQHCPDF
llYOUI+SpLJ6/vURRbHSnnn8a/XG+nzedGH5JGqEJNQsz+xT2axM0/W/CRknmGaJ
kda/AoGANWrLCz708y7VYgAtW2Uf1DPOIYMdvo6fxIB5i9ZfISgcJ/bbCUkFrhoH
+vq/5CIWxCPp0f85R4qxxQ5ihxJ0YDQT9Jpx4TMss4PSavPaBH3RXow5Ohe+bYoQ
NE5OgEXk2wVfZczCZpigBKbKZHNYcelXtTt/nP3rsCuGcM4h53s=
-----END RSA PRIVATE KEY-----
```

The VMs have a *vagrant* user, for compatibility with Vagrant setups,
which has password-less sudo access to the root account. The above SSH
key provides access to the *vagrant* user (for compatibility), as well
as the *root* user directly.

The firewall configuration set up by *vmine* does not allow direct
access from the public Internet to the virtual machines, as a
consequence of using well-known authorization credentials. To connect
to the virtual machines, you're expected to jump through the VM host.


## HTTP API

The HTTP API accepts POST requests with a JSON-encoded payload.

### */api/create-group*

Creates a new group of VMs, which will be assigned a unique ID.

Sample request:

```json
{
    "network": "10.12.13.0/24",
    "ttl": "1h",
    "hosts": [
        {
            "name": "host1",
            "ip": "10.12.13.100",
            "image": "buster",
            "ram": 4096,
            "cpu": 1
        },
        ...
    ]
}
```

Most parameters except for the network configuration can be omitted,
and will use defaults. A configuration that fails to validate
(because, for instance, a host's IP address is not contained in the
specified network, or the network is already used by another VM group,
etc.) will result in a 400 error. A successful response will contain
the unique ID of the newly created group, e.g.:

```json
{
    "group_id": "0fb1"
}
```

The ID should be treated as an opaque token.

### */api/stop-group*

Stops a running VM group, given its unique ID.

Sample request:

```json
{
    "group_id": "0fb1"
}
```

No response is returned.

## Quick Testing

Clone this repository locally, then build the code (set GO111MODULE=on
if you have a Go version older than 1.15):

```shell
$ go build ./cmd/vmine
```

Build a VM image, for example a Debian stable (Buster) one, following
the instructions in the "Building an image" section above. Then create
a copy of *images.yml* and modify it by pointing it at the full paths
for the image, the kernel, and the initrd you just generated:

```shell
$ cp images.yml my-images.yml
$ vi my-images.yml
```

Run the server as root:

```shell
$ sudo ./vmine --images=my-images.yml
```

To start a few VMs you can then make a HTTP request to the API using
*curl*:

```
$ curl -X POST -d '{"network": "10.12.32.0/24", "hosts": [{"ip": "10.12.32.10"}, {"ip": "10.12.32.11"}]}' \
      http://localhost:4949/api/create-group
```

Ctrl-C will shut down the server, stop any running VMs, and clear up
any changes to the network and iptables configuration.


## Implementation notes

All VMs are children of the main vmine process and are stopped when it
exits. Centralizing process control allows us to clean up neatly all
associated resources (network devices, firewall rules) when a VM
exits.

Vmine allows a limited amount of run-time configuration, mostly
limited to network configuration. We do this via kernel boot
parameters, which are then parsed by a script that performs runtime
setup at boot. This is one of the reasons (speed being the other) for
booting the image with an external kernel, rather than via BIOS and
GRUB (in that scenario we would need DHCP and some sort of network
metadata service, AWS-style -- this is a lot simpler).
