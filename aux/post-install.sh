#!/bin/sh

TARGET="$1"
if [ -z "$TARGET" ]; then
    echo "Usage: $0 TARGET" >&2
    exit 2
fi

set -e
set -x

SSH_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key"

# Install required packages. We need an SSH server and a Python
# installation for Ansible. In order to guess the "reasonable" version
# of Python that Ansible will be looking for by default, we check for
# the availability of the "python-apt" package: if it exists, we're on
# a Python 2-based distro (Buster), otherwise we're going to assume
# Python 3. This avoids having to hard-code an explicit distro check.
PACKAGES="openssh-server sudo ca-certificates"
has_python_apt=$(chroot $TARGET apt-cache search --names-only '^python-apt$' | wc -l)
if [ $has_python_apt -eq 1 ]; then
    PACKAGES="$PACKAGES python python-apt"
else
    PACKAGES="$PACKAGES python3 python3-apt"
fi

chroot $TARGET apt-get install -y $PACKAGES

# Install and configure SSH with a key for the root user.
# Also set up a 'vagrant' user for easy Vagrant compatibility.

mkdir -p $TARGET/root/.ssh
echo "$SSH_KEY" \
     > $TARGET/root/.ssh/authorized_keys

chroot $TARGET adduser --quiet --disabled-password vagrant </dev/null
echo "vagrant ALL=(ALL) NOPASSWD:ALL" \
     > $TARGET/etc/sudoers.d/vagrant
mkdir -p $TARGET/home/vagrant/.ssh
echo "$SSH_KEY" \
     > $TARGET/home/vagrant/.ssh/authorized_keys
chmod 0700 $TARGET/home/vagrant/.ssh
chroot $TARGET chown -R vagrant:vagrant /home/vagrant/.ssh


# Install the network configuration script.
cat > $TARGET/usr/sbin/vmine-network-config <<EOF
#!/bin/sh

DEVICE="\${1:-eth0}"

for arg in \$(cat /proc/cmdline); do
  case "\$arg" in
    vmine_ip=*)
      ip=\$(echo "\${arg#*=}" | cut -d: -f1)
      gw=\$(echo "\${arg#*=}" | cut -d: -f2)
      ip addr add "\$ip" dev \$DEVICE
      ip route add default via "\$gw"
      ;;
    vmine_dns=*)
      resolver="\${arg#*=}"
      echo "nameserver \${resolver}" > /etc/resolv.conf
      ;;
    vmine_hostname=*)
      hostname="\${arg#*=}"
      echo "\${hostname}" > /etc/hostname
      hostname "\${hostname}"
      echo "127.0.0.1 \${hostname}" >> /etc/hosts
      ;;
  esac
done

EOF

chmod 755 $TARGET/usr/sbin/vmine-network-config

# Configure ifupdown to start our script for eth0. This overwrites the
# default eth0 dhcp configuration made by autopkgtest.
cat > $TARGET/etc/network/interfaces.d/eth0 <<EOF
auto eth0
iface eth0 inet manual
    up /usr/sbin/vmine-network-config eth0
EOF

# Autopkgtest installs a root shell on tty1, which we can happily live
# without.
rm -f $TARGET/etc/systemd/system/autopkgtest.service

exit 0
