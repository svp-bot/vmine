#!/bin/sh

set -e
set -u

# Racy but good enough
get_free_nbd() {
    for dev in /dev/nbd?; do
        if ! lsof $dev > /dev/null; then
            echo $dev
            return
        fi
    done
    echo "ERROR: could not find an available nbd device" >&2
    exit 1
}

if [ "$#" -lt 2 ]; then
    echo "$0 usage: IMAGE OUTPATH"
    echo "   Extract kernel/initrd from IMAGE into OUTPATH"
    exit 1
fi

image=$1
if [ ! -e "$image" ]; then
    echo "ERROR: image file '$image' does not exist" >&2
    exit 1
fi

outpath=$2
if [ ! -d "$outpath" ]; then
    echo "ERROR: output path '$outpath' does not exist or is not a directory" >&2
    exit 1
fi

# Ensure the nbd module is loaded.
if ! grep -q '^nbd ' /proc/mounts; then
    modprobe nbd
fi

# Find a free NBD device and mount it on a temporary directory using
# qemu-nbd.
dev=$(get_free_nbd)
mntpoint=$(mktemp -d)

trap "{
   set +e ; umount $mntpoint ; rmdir $mntpoint ;
   qemu-nbd --disconnect $dev >/dev/null ;
}" INT EXIT

qemu-nbd --connect=$dev $image

# Wait a little for ndbXp1 to appear.
counter=0
while [ $counter -lt 10 ]; do
    [ -e "${dev}p1" ] && break
    counter=$(expr $counter + 1)
    sleep 1
done
if [ ! -e "${dev}p1" ]; then
    echo "ERROR: ${dev}p1 does not exist" >&2
    exit 1
fi

mount ${dev}p1 $mntpoint

for i in vmlinuz initrd.img; do
    cp -v ${mntpoint}/$(readlink ${mntpoint}/$i) $outpath
done
