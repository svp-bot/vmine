package vmine

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

// HTTPEngine wraps an Engine with an HTTP API. It implements the
// http.Handler interface.
type HTTPEngine struct {
	*Engine
}

func NewHTTPEngine(e *Engine) *HTTPEngine {
	return &HTTPEngine{Engine: e}
}

func (h *HTTPEngine) handleCreateGroup(w http.ResponseWriter, r *http.Request) {
	var req CreateGroupRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.Engine.CreateGroup(&req)
	if err != nil {
		log.Printf("CreateGroup error: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp) // nolint: errcheck
}

func (h *HTTPEngine) handleStopGroup(w http.ResponseWriter, r *http.Request) {
	var req StopGroupRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err := h.Engine.StopGroup(req.GroupID)
	if err != nil {
		log.Printf("StopGroup error: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, "{}") // nolint: errcheck
}

func (h *HTTPEngine) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch {
	case r.URL.Path == "/api/create-group" && r.Method == "POST":
		h.handleCreateGroup(w, r)
	case r.URL.Path == "/api/stop-group" && r.Method == "POST":
		h.handleStopGroup(w, r)
	default:
		http.NotFound(w, r)
	}
}
