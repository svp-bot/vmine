package vmine

import (
	"flag"
	"fmt"
	"net"
)

var (
	vmUser      = flag.String("user", "nobody", "user that the qemu processes should run as")
	dnsResolver = flag.String("resolver", "8.8.8.8", "DNS resolver address for VMs")
)

func attachInterfaceToBridge(brDev, tapDev string) SetupTask {
	return newShellSetupTask(
		"ip link set dev {{.Dev}} master {{.Bridge}}",
		"ip link set dev {{.Dev}} nomaster",
		map[string]string{
			"Dev":    tapDev,
			"Bridge": brDev,
		},
	)
}

func allowForwarding(dev string) SetupTask {
	return newShellSetupTask(
		"iptables -A FORWARD -i {{.Dev}} -j ACCEPT && iptables -A FORWARD -o {{.Dev}} -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT",
		"iptables -D FORWARD -i {{.Dev}} -j ACCEPT && iptables -D FORWARD -o {{.Dev}} -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT",
		map[string]string{
			"Dev": dev,
		},
	)
}

func masquerade(outDev, network string) SetupTask {
	return newShellSetupTask(
		"iptables -t nat -A POSTROUTING -s {{.SrcNet}} \\! -o {{.OutDev}} -j MASQUERADE",
		"iptables -t nat -D POSTROUTING -s {{.SrcNet}} \\! -o {{.OutDev}} -j MASQUERADE",
		map[string]string{
			"SrcNet": network,
			"OutDev": outDev,
		},
	)
}

func createBridgeInterface(dev, netaddr string) SetupTask {
	return newShellSetupTask(
		"ip link add name {{.Dev}} type bridge && ip link set {{.Dev}} up && ip addr add {{.IP}} dev {{.Dev}}",
		"ip link delete {{.Dev}} type bridge",
		map[string]string{
			"Dev": dev,
			"IP":  netaddr,
		},
	)
}

func createTapInterface(dev string) SetupTask {
	return newShellSetupTask(
		"ip tuntap add dev {{.Dev}} mode tap user {{.User}} && ip link set {{.Dev}} up",
		"ip tuntap delete dev {{.Dev}} mode tap",
		map[string]string{
			"Dev":  dev,
			"User": *vmUser,
		},
	)
}

// Enable the telnetPort when debugging image builds.
//var telnetPort = 2020

func runVM(vm *VM, network *Network) RunnableTask {
	bootArgs := fmt.Sprintf(
		"console=ttyS0,9600n8 root=/dev/vda1 net.ifnames=0 ro vmine_hostname=%s vmine_ip=%s:%s vmine_dns=%s",
		vm.Hostname,
		addrCIDR(vm.Addr, network.Network),
		network.GatewayAddr.String(),
		*dnsResolver,
	)

	//telnetPort++

	return newCmd(
		"/usr/bin/qemu-system-x86_64",
		"-name", "guest="+vm.Hostname+",debug-threads=on",
		"-no-user-config",
		"-nodefaults",
		"-no-reboot",
		"-nographic",
		"-enable-kvm",
		"-runas", *vmUser,
		"-cpu", "host",
		"-machine", "q35,accel=kvm,usb=off,dump-guest-core=off",
		"-realtime", "mlock=off",
		"-rtc", "base=utc",
		"-display", "none",
		"-object", "rng-random,id=objrng0,filename=/dev/urandom",
		"-device", "virtio-rng-pci,rng=objrng0,id=rng0",
		"-sandbox", "on,obsolete=deny,resourcecontrol=deny",
		// Add storage.
		"-snapshot",
		"-drive", "driver=qcow2,node-name=vda,if=virtio,file.driver=file,file.filename="+vm.Image.Path,
		// Add network device.
		"-netdev", "tap,id=n0,script=no,downscript=no,ifname="+vm.NetDevice,
		"-device", "virtio-net-pci,netdev=n0,mac="+vm.MAC,
		// Memory / CPU.
		"-m", fmt.Sprintf("%dM", vm.RAM),
		"-smp", fmt.Sprintf("%d", vm.CPU),
		// Serial console for debugging.
		//"-serial", "file:"+vm.Hostname+".log",
		//"-serial", fmt.Sprintf("telnet:127.0.0.1:%d,server,nowait", telnetPort),
		// Boot parameters.
		"-kernel", vm.Image.KernelPath,
		"-initrd", vm.Image.InitrdPath,
		"-append", bootArgs,
	)
}

// Combine an IP and a network mask in CIDR format.
func addrCIDR(addr net.IP, ipnet *net.IPNet) string {
	nn := net.IPNet{
		IP:   addr,
		Mask: ipnet.Mask,
	}
	return nn.String()
}
